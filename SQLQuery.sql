CREATE DATABASE C5Challenge;

USE C5Challenge;

CREATE TABLE PermissionTypes (
    Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
    Descripcion NVARCHAR(100) NOT NULL
);

CREATE TABLE Permissions (
    Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
    NombreEmpleado NVARCHAR(100) NOT NULL,
    ApellidoEmpleado NVARCHAR(100) NOT NULL,
    TipoPermiso INT NOT NULL FOREIGN KEY (TipoPermiso) REFERENCES PermissionTypes(Id),
    FechaPermiso DATE NOT NULL
);
