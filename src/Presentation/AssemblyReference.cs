﻿using System.Reflection;

namespace Presentation;

/// <summary>
/// Represents the presentation assembly.
/// </summary>
public static class AssemblyReference
{
    /// <summary>
    /// The presentation assembly.
    /// </summary>
    public static readonly Assembly Assembly = Assembly.GetExecutingAssembly();
}
