﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Presentation.Extensions;
using Presentation.Infrastructure;
using Domain.Core.Primitives.Result;
using Application.Contracts;
using Application.Queries.GetPermissions;
using Application.Commands.ModifyPermissions;

namespace Presentation.Controllers;

[Route("api/v1/permissions")]
[ApiController]
public sealed class PermissionController : ApiController
{
    public PermissionController(IMediator mediator)
        : base(mediator)
    {
    }

    [HttpGet("")]
    [ProducesResponseType(typeof(List<PermissionsResponse>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetListofPermissions([FromQuery] int? id, CancellationToken cancellationToken)
    {
        return await Mediator
            .Send(new GetPermissionsQuery(id), cancellationToken)
            .Map(Ok);
    }

    [HttpPost("")]
    [ProducesResponseType(typeof(Result), StatusCodes.Status200OK)]
    public async Task<IActionResult> InsertPermissions([FromBody] PermissionsRequest permissionRequest)
    {
        return await Result.Create(permissionRequest, Domain.Core.Errors.DomainErrors.General.UnProcessableRequest)
            .Ensure(request => !string.IsNullOrEmpty(request.NombreEmpleado), Domain.Core.Errors.DomainErrors.PermissionRequest.NameNullOrEmpty)
            .Ensure(request => !string.IsNullOrEmpty(request.ApellidoEmpleado), Domain.Core.Errors.DomainErrors.PermissionRequest.LastNameNullOrEmpty)
            .Ensure(request => request.TipoPermiso > decimal.Zero, Domain.Core.Errors.DomainErrors.PermissionRequest.PermissionTypeNullOrEmpty)
            .Map(request => new RequestPermissionCommand(
                request.ApellidoEmpleado,
                request.NombreEmpleado,
                request.TipoPermiso))
            .Bind(command => Mediator.Send(command))
            .Match(Ok, BadRequest);
    }

    [HttpPatch("{id}")]
    [ProducesResponseType(typeof(Result), StatusCodes.Status200OK)]
    public async Task<IActionResult> PatchPermissions(int id, [FromBody] PermissionsRequest permissionRequest)
    {
        return await Result.Create(permissionRequest, Domain.Core.Errors.DomainErrors.General.UnProcessableRequest)
            .Ensure(request => id > decimal.Zero, Domain.Core.Errors.DomainErrors.PermissionRequest.PermissionIdInvalid)
            .Map(request => new ModifyPermissionCommand(
                id,
                request.TipoPermiso,
                request.NombreEmpleado,
                request.ApellidoEmpleado))
            .Bind(command => Mediator.Send(command))
            .Match(Ok, BadRequest);
    }
}