﻿using Domain.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Modules;

public partial class PermissionType : Entity
{
    public string Descripcion { get; set; }

    public virtual ICollection<Permission> Permissions { get; set; }
}
