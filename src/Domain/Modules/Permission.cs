﻿using Domain.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Modules;

public partial class Permission : Entity
{
    public string NombreEmpleado { get; set; }

    public string ApellidoEmpleado { get; set; }

    public int TipoPermiso { get; set; }

    public DateTime FechaPermiso { get; set; }

    public virtual PermissionType TipoPermisoNavigation { get; set; }
}
