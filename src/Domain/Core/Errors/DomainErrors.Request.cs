﻿using Domain.Core.Primitives;

namespace Domain.Core.Errors;

/// <summary>
/// Contains the domain errors.
/// </summary>
public static partial class DomainErrors
{
    /// <summary>
    /// Contains the Request errors.
    /// </summary>
    public static class PermissionRequest
    {
        /// <summary>
        /// Gets the Permission id invalid error.
        /// </summary>
        public static Error PermissionIdInvalid => new("Request.PermissionIdInvalid", "Permission id is invalid.");

        /// <summary>
        /// Gets the Permission Not Founderror.
        /// </summary>
        public static Error PermissionNotFound => new("Request.PermissionNotfound", "The requested Permission was not found..");

        public static Error NameNullOrEmpty => new("Request.NameNullOrEmpty", "Employee Name is required.");

        public static Error LastNameNullOrEmpty => new("Request.LastNameNullOrEmpty", "Employee Last Name is required.");

        public static Error PermissionTypeNullOrEmpty => new("Request.PermissionTypeNullOrEmpty", "Permission Type is required.");
    }
}
