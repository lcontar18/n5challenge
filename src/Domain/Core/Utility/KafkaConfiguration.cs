﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Core.Utility;

public class KafkaConfiguration
{
    public string BootstrapServers { get; set; }

    public string Topic { get; set; }
}
