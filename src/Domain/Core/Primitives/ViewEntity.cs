﻿using Domain.Core.Utility;

namespace Domain.Core.Primitives;

/// <summary>
/// Represents the base class that all database views derive from.
/// </summary>
public abstract class ViewEntity : IEquatable<ViewEntity>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ViewEntity"/> class.
    /// </summary>
    /// <remarks>
    /// Required by EF Core.
    /// </remarks>
    protected ViewEntity()
    {
    }

    public static bool operator ==(ViewEntity a, ViewEntity b)
    {
        if (a is null && b is null)
        {
            return true;
        }

        if (a is null || b is null)
        {
            return false;
        }

        return a.Equals(b);
    }

    public static bool operator !=(ViewEntity a, ViewEntity b)
    {
        return !(a == b);
    }

    /// <inheritdoc />
    public bool Equals(ViewEntity other)
    {
        if (other is null)
        {
            return false;
        }

        return ReferenceEquals(this, other);
    }

    /// <inheritdoc />
    public override bool Equals(object obj)
    {
        if (obj is null)
        {
            return false;
        }

        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj.GetType() != GetType())
        {
            return false;
        }

        if (obj is not ViewEntity)
        {
            return false;
        }

        return true;
    }

    public override int GetHashCode()
    {
        return this.GetHashCode() * 41;
    }
}
