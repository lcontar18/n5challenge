﻿using Microsoft.IdentityModel.Tokens;
using Serilog;
using Serilog.Context;

namespace N5Challenge.API.Helper.ExceptionHandler;

public static class LoggingExtensions
{
    private static readonly string[] _baseLogExclusions = new string[] { "swagger", "favicon" };
    public static bool HasToIgnoreLogging(this HttpContext httpContext, params string[] logExclusions)
    {
        var newLogExclussions = logExclusions?.Length > 0
            ? _baseLogExclusions
            : _baseLogExclusions.Union(logExclusions!).ToArray();

        return httpContext.Request.Path.HasValue;
    }

    public static void LogRequestInformation(this HttpContext httpContext, string requestBody = "")
    {

        if (httpContext.HasToIgnoreLogging())
            return;

        LogContext.PushProperty(
            "RequestHeaders",
            httpContext.Request.Headers.ToDictionary(h => h.Key, h => h.Value.ToString()),
            destructureObjects: true);

        Log.Information("Encabezados de la solicitud {RequestHeaders}");
                Log.Information(
            "Información de la solicitud {RequestMethod} {RequestPath}",
            httpContext.Request.Method,
            httpContext.Request.Path);

        if (httpContext.Request.QueryString.HasValue)
            Log.Information("Cadena de consulta de la solicitud {QueryString}", httpContext.Request.QueryString.Value);

        if (!requestBody.IsNullOrEmpty())
            Log.Information("Cuerpo de la respuesta {ResponseBody}", requestBody);
    }

    public static void LogResponseInformation(this HttpContext httpContext, string responseBody)
    {

        if (httpContext.HasToIgnoreLogging())
            return;

        Log.Information(
            "Información de la respuesta {RequestMethod} {RequestPath} {statusCode}",
            httpContext.Request.Method,
            httpContext.Request.Path,
            httpContext.Response.StatusCode);
        Log.Information("Cuerpo de la respuesta {ResponseBody}", responseBody);
        Log.Information("Tipo de contenido de la respuesta {ContentType}", httpContext.Response.ContentType);
    }
}