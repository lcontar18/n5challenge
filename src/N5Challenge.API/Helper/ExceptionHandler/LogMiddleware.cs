﻿using Serilog;
using System.Text;

namespace N5Challenge.API.Helper.ExceptionHandler;

public class LogMiddleware
{
    private readonly RequestDelegate _next;

    public LogMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext httpContext)
    {
        var requestStartedAt = DateTime.Now;
        Log.Logger = new LoggerConfiguration()
        .MinimumLevel.Verbose()
        .Enrich.FromLogContext()
        .WriteTo.Console()
        .CreateLogger();
        // Leave the body open so the next middleware can read it.
        httpContext.Request.EnableBuffering();

        await ReadAndLogRequest(httpContext);
        await ReadAndLogResponse(httpContext);

        double elapsedTime = (DateTime.Now - requestStartedAt).TotalMilliseconds;
        string requestWasCompleted = "Request was completed in {" + elapsedTime + "}ms";
        Log.Information("{message}", requestWasCompleted);

    }

    private static async Task ReadAndLogRequest(HttpContext httpContext)
    {
        var requestContentLength = Convert.ToInt32(httpContext.Request.ContentLength);

        if (requestContentLength <= 0)
        {
            httpContext.LogRequestInformation();
            return;
        }

        byte[] buffer = new byte[requestContentLength];

        // Leave the body open so the next middleware can read it.
        using var reader = new StreamReader(
            httpContext.Request.Body,
            encoding: Encoding.UTF8,
            detectEncodingFromByteOrderMarks: false,
            bufferSize: buffer.Length,
            leaveOpen: true);

        var requestBody = await reader.ReadToEndAsync();

        httpContext.LogRequestInformation(requestBody);

        // Reset the request body stream position so the next middleware can read it
        httpContext
            .Request
            .Body
            .Position = 0;
    }

    private async Task ReadAndLogResponse(HttpContext httpContext)
    {
        // Hold a reference to the original response body stream.
        var originalResponseBodyReference = httpContext.Response.Body;

        // Target the response body to a new memory stream.
        var responseBodyMemoryStream = new MemoryStream();
        httpContext.Response.Body = responseBodyMemoryStream;

        // Handle the request.
        await HandleRequest(httpContext);

        // Read the response body.
        httpContext.Response.Body.Seek(0, SeekOrigin.Begin);
        var responseBody = await new StreamReader(httpContext.Response.Body).ReadToEndAsync();
        httpContext.Response
            .Body
            .Seek(0, SeekOrigin.Begin);

        // Log information
        httpContext.LogResponseInformation(responseBody);

        // Copy the response in the memory stream to the original response stream.
        await responseBodyMemoryStream.CopyToAsync(originalResponseBodyReference);
    }

    private async Task HandleRequest(HttpContext httpContext)
        => await _next(httpContext);
}