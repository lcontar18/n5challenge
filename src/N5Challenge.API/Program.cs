using N5Challenge.API.Configuration;
using N5Challenge.API.Helper.ExceptionHandler;
using Serilog;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
DotNetEnv.Env.Load();

builder.Services
    .InstallServices(
    builder.Configuration,
    typeof(IServiceInstaller).Assembly);

builder.Host.UseSerilog((context, configuration) =>
    configuration.ReadFrom.Configuration(context.Configuration));

builder.Services.AddSwaggerGen();

var appName = builder.Environment.ApplicationName;

var app = builder.Build();


if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Verbose()
            .Enrich.FromLogContext()
            .WriteTo.Console()
            .CreateLogger();

Log.Information("Starting...");

try
{
    app.UseSerilogRequestLogging();

    app.UseMiddleware<LogMiddleware>();

    app.UseHttpsRedirection();

    app.UseRouting();

    app.UseEndpoints(endpoints => endpoints.MapControllers());

    Log.Information("Starting web host ({ApplicationContext})...", appName);

    app.Run();

}
catch (Exception ex)
{
    Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", appName);
}
finally
{
    Log.CloseAndFlush();
}