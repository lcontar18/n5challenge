﻿using Application.Core.Abstractions.Data;
using Application.Services;
using Domain.Core.Utility;
using EntityFrameworkCore.UseRowNumberForPaging;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using N5Challenge.API.Configuration;
using IElasticsearchService = Application.Services.IElasticsearchService;

namespace N5Challenge.API.ServiceInstallers.Infrastructure;

/// <summary>
/// Represents the infrastructure services installer.
/// </summary>
public class InfrastructureServiceInstaller : IServiceInstaller
{
    public void Install(IServiceCollection services, IConfiguration configuration)
    {
        var rrhhConnectionString = Environment.GetEnvironmentVariable("CHALLENGE_CONNECTION_STRING");
        var elasticConnectionString = Environment.GetEnvironmentVariable("ELASTIC_CONNECTION_STRING");
        services.AddDbContext<ChallengeDbContext>(options => {options.UseSqlServer(rrhhConnectionString, i => i.UseRowNumberForPaging());});
        services.AddScoped<IChallengeDbContext>(serviceProvider => serviceProvider.GetRequiredService<ChallengeDbContext>());
        services.AddScoped<IChallengeUnitOfWork>(serviceProvider => serviceProvider.GetRequiredService<ChallengeDbContext>());

        services.AddSingleton<string>(elasticConnectionString);
        services.AddSingleton<IElasticsearchService>(sp => new ElasticsearchService(elasticConnectionString));

        services.Configure<KafkaConfiguration>(configuration.GetSection("Kafka"));


    }
}