﻿using N5Challenge.API.Configuration;
using Presentation;

namespace N5Challenge.API.ServiceInstallers.Presentation;

/// <summary>
/// Represents the presentation services installer.
/// </summary>
public sealed class PresentationServiceInstaller : IServiceInstaller
{
    public void Install(IServiceCollection services, IConfiguration configuration)
    {
        services.ConfigureOptions<ApiBehaviorOptionsSetup>();

        //services.ConfigureOptions<MvcOptionsSetup>();

        services.AddControllers().AddApplicationPart(AssemblyReference.Assembly);

        services.AddHttpContextAccessor();

        services.AddOptions();
    }
}
