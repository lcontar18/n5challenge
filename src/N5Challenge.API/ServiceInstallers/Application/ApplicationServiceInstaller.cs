﻿using Application;
using Application.Services;
using Infrastructure;
using N5Challenge.API.Configuration;

namespace N5Challenge.API.ServiceInstallers.Application;

public class ApplicationServiceInstaller : IServiceInstaller
{
    public void Install(IServiceCollection services, IConfiguration configuration)
    {
        services.AddMediatR(c => c.RegisterServicesFromAssembly(AssemblyReference.Assembly));

        services.AddScoped<IElasticsearchService, ElasticsearchService>();
        services.AddScoped<IKafkaProducerService, KafkaProducerService>();
    }
}
