﻿using System.Reflection;

namespace Application;

/// <summary>
/// Represents the application assembly.
/// </summary>
public static class AssemblyReference
{
    /// <summary>
    /// The application assembly.
    /// </summary>
    public static readonly Assembly Assembly = Assembly.GetExecutingAssembly();
}
