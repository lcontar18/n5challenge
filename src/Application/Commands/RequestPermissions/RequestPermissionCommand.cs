﻿using Application.Contracts;
using Application.Core.Abstractions.Messaging;
using Domain.Core.Primitives.Result;

namespace Application.Commands.ModifyPermissions;

public sealed class RequestPermissionCommand : ICommand<Result>
{
    public string ApellidoEmpleado { get; set; }

    public string NombreEmpleado { get; set; }

    public int TipoPermiso { get; set; }

    public RequestPermissionCommand(string apellidoEmpleado, string nombreEmpleado, int tipoPermiso)
    {
        ApellidoEmpleado = apellidoEmpleado;
        NombreEmpleado = nombreEmpleado;
        TipoPermiso = tipoPermiso;
    }
}
