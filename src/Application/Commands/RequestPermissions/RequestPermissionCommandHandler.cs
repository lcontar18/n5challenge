﻿using Application.Contracts;
using Application.Core.Abstractions.Data;
using Application.Core.Abstractions.Messaging;
using Application.Services;
using Domain.Core.Errors;
using Domain.Core.Primitives.Result;
using Domain.Modules;
using Microsoft.EntityFrameworkCore;

namespace Application.Commands.ModifyPermissions;

public sealed class RequestPermissionCommandHandler : ICommandHandler<RequestPermissionCommand, Result>
{
    private readonly IChallengeDbContext dbContext;
    private readonly IChallengeUnitOfWork unitOfWork;
    private readonly IElasticsearchService elasticsearchService;
    private readonly IKafkaProducerService kafkaProducerService;

    public RequestPermissionCommandHandler(IChallengeDbContext dbContext, IChallengeUnitOfWork unitOfWork, IElasticsearchService elasticsearchService, IKafkaProducerService kafkaProducerService)
    {
        this.dbContext = dbContext;
        this.unitOfWork = unitOfWork;
        this.elasticsearchService = elasticsearchService;
        this.kafkaProducerService = kafkaProducerService;
    }

    public async Task<Result> Handle(RequestPermissionCommand request, CancellationToken cancellationToken)
    {
        var permission = new Permission()
        {
            NombreEmpleado = request.NombreEmpleado,
            ApellidoEmpleado = request.ApellidoEmpleado,
            TipoPermiso = request.TipoPermiso,
            FechaPermiso = DateTime.UtcNow
        };

        try
        {
            this.dbContext.Insert(permission);
            await this.unitOfWork.SaveChangesAsync(cancellationToken);
            await this.elasticsearchService.InsertPermissionIndexAsync(permission);
            await kafkaProducerService.ProduceMessageAsync(Guid.NewGuid(), "request");
            return Result.Success();
        }
        catch (Exception)
        {
            return Result.Failure(DomainErrors.General.UnProcessableRequest);
        }
    }
}
