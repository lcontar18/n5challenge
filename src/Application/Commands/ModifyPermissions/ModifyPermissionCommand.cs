﻿using Application.Contracts;
using Application.Core.Abstractions.Messaging;
using Domain.Core.Primitives.Result;

namespace Application.Commands.ModifyPermissions;

public sealed class ModifyPermissionCommand : ICommand<Result>
{
    public int Id { get; set; }

    public string? NombreEmpleado { get; set; }

    public string? ApellidoEmpleado { get; set; }

    public int? TipoPermiso { get; set; }

    public ModifyPermissionCommand(int id, int tipoPermiso, string nombreEmpleado, string apellidoEmpleado)
    {
        Id = id;
        TipoPermiso = tipoPermiso;
        NombreEmpleado = nombreEmpleado;
        ApellidoEmpleado = apellidoEmpleado;
    }
}
