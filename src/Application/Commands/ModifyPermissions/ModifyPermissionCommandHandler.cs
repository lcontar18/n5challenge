﻿using Application.Contracts;
using Application.Core.Abstractions.Data;
using Application.Core.Abstractions.Messaging;
using Application.Services;
using Domain.Core.Errors;
using Domain.Core.Primitives.Result;
using Domain.Modules;
using Microsoft.EntityFrameworkCore;
using System.Security;

namespace Application.Commands.ModifyPermissions;

public sealed class ModifyPermissionCommandHandler : ICommandHandler<ModifyPermissionCommand, Result>
{
    private readonly IChallengeDbContext dbContext;
    private readonly IChallengeUnitOfWork unitOfWork;
    private readonly IElasticsearchService elasticsearchService;
    private readonly IKafkaProducerService kafkaProducerService;

    public ModifyPermissionCommandHandler(IChallengeDbContext dbContext, IChallengeUnitOfWork unitOfWork, IElasticsearchService elasticsearchService, IKafkaProducerService kafkaProducerService)
    {
        this.dbContext = dbContext;
        this.unitOfWork = unitOfWork;
        this.elasticsearchService = elasticsearchService;
        this.kafkaProducerService = kafkaProducerService;
    }

    public async Task<Result> Handle(ModifyPermissionCommand request, CancellationToken cancellationToken)
    {
        if (request.Id == 0)
        {
            return Result.Failure(DomainErrors.PermissionRequest.PermissionIdInvalid);
        }

        var existingPermission = await this.dbContext.Set<Permission>().FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

        if (existingPermission == null)
        {
            return Result.Failure(DomainErrors.PermissionRequest.PermissionNotFound);
        }

        if (request.NombreEmpleado != null)
        {
            existingPermission.NombreEmpleado = request.NombreEmpleado;
        }

        if (request.ApellidoEmpleado != null)
        {
            existingPermission.ApellidoEmpleado = request.ApellidoEmpleado;
        }

        if (request.TipoPermiso.HasValue)
        {
            existingPermission.TipoPermiso = request.TipoPermiso.Value;
            existingPermission.FechaPermiso = DateTime.UtcNow;
        }

        try
        {
            await this.unitOfWork.SaveChangesAsync(cancellationToken);
            await this.elasticsearchService.InsertPermissionIndexAsync(existingPermission);
            await kafkaProducerService.ProduceMessageAsync(Guid.NewGuid(), "modify");
            return Result.Success();
        }
        catch (Exception)
        {
            return Result.Failure(DomainErrors.General.UnProcessableRequest);
        }
    }
}
