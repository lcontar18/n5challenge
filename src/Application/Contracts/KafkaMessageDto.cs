﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Contracts;

public class KafkaMessageDto
{
    public Guid OperationId { get; set; }
    public string OperationName { get; set; }
}

