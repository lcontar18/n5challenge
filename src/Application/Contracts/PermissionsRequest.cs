﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Contracts;

public sealed record PermissionsRequest
(
    string ApellidoEmpleado,
    string NombreEmpleado,
    int TipoPermiso
);
