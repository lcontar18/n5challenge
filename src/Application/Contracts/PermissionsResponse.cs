﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Contracts;

public sealed record PermissionsResponse
(
    int Id,
    string NombreEmpleado,
    string ApellidoEmpleado,
    int TipoPermiso,
    DateTime FechaPermiso
);
