﻿using Domain.Core.Primitives;
using Domain.Core.Primitives.Maybe;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace Application.Core.Abstractions.Data;

/// <summary>
/// Represents the application database context interface.
/// </summary>
public interface IChallengeDbContext : IDbContext
{
    /// <summary>
    /// Gets the database set for the specified entity type.
    /// </summary>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <returns>The database set for the specified entity type.</returns>
    DbSet<TEntity> Set<TEntity>()
        where TEntity : Entity;

    /// <summary>
    /// Gets the database set for the specified view entity type.
    /// </summary>
    /// <typeparam name="TVwEntity">The view entity type.</typeparam>
    /// <returns>The database set for the specified view entity type.</returns>
    DbSet<TVwEntity> SetVw<TVwEntity>()
        where TVwEntity : ViewEntity;

    /// <summary>
    /// Gets the entity with the specified identifier.
    /// </summary>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <param name="id">The entity identifier.</param>
    /// <returns>The <typeparamref name="TEntity"/> with the specified identifier if it exists, otherwise null.</returns>
    Task<Maybe<TEntity>> GetBydIdAsync<TEntity>(int id)
        where TEntity : Entity;

    /// <summary>
    /// Inserts the specified entity into the database.
    /// </summary>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <param name="entity">The entity to be inserted into the database.</param>
    void Insert<TEntity>(TEntity entity)
        where TEntity : Entity;

    /// <summary>
    /// Inserts the specified entities into the database.
    /// </summary>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <param name="entities">The entities to be inserted into the database.</param>
    void InsertRange<TEntity>(IReadOnlyCollection<TEntity> entities)
        where TEntity : Entity;

    /// <summary>
    /// Removes the specified entity from the database.
    /// </summary>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <param name="entity">The entity to be removed from the database.</param>
    void Remove<TEntity>(TEntity entity)
        where TEntity : Entity;

    /// <summary>
    /// Executes the specified SQL command asynchronously and returns the number of affected rows.
    /// </summary>
    /// <param name="sql">The SQL command.</param>
    /// <param name="parameters">The parameters collection.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>The number of rows affected.</returns>
    Task<int> ExecuteSqlAsync(string sql, IEnumerable<SqlParameter> parameters, CancellationToken cancellationToken = default);

    /// <summary>
    /// Executes the specified stored procedure asynchronously and returns the result set.
    /// </summary>
    /// <typeparam name="T">The type of the result set.</typeparam>
    /// <param name="storedProcedure">The stored procedure to execute.</param>
    /// <param name="parameters">The parameters for the stored procedure.</param>
    /// <returns>A list of results of type T.</returns>
    Task<List<T>> ExecuteStoredProcedureAsync<T>(string storedProcedure, List<Microsoft.Data.SqlClient.SqlParameter> parameters) where T : class, new();
}
