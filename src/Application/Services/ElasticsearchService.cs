﻿namespace Application.Services;

using System;
using System.Threading.Tasks;
using Nest;
using Domain.Modules;
using Application.Contracts;

public class ElasticsearchService : IElasticsearchService
{
    private readonly ElasticClient elasticClient;

    public ElasticsearchService(string elasticsearchUri)
    {
        var uri = new Uri(elasticsearchUri);
        var settings = new ConnectionSettings(uri);
        elasticClient = new ElasticClient(settings);
    }

    public async Task InsertPermissionIndexAsync(Permission permission)
    {
        var indexResponse = await elasticClient.IndexAsync(permission, idx => idx.Index("permissions"));
        if (!indexResponse.IsValid)
        {
            throw new Exception($"Error al indexar en Elasticsearch: {indexResponse.DebugInformation}");
        }
    }

    public async Task InsertPermissionListIndexAsync(List<PermissionsResponse> permissions)
    {
        var bulkIndexResponse = await elasticClient.IndexManyAsync(permissions, "permissions");
        if (!bulkIndexResponse.IsValid)
        {
            throw new Exception($"Error al indexar en Elasticsearch: {bulkIndexResponse.DebugInformation}");
        }
    }
}


