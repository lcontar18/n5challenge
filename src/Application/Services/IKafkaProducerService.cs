﻿using Application.Contracts;
using Domain.Modules;

namespace Application.Services;

public interface IKafkaProducerService
{
   Task ProduceMessageAsync(Guid operationId, string operationName);
}
