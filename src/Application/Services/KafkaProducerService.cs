﻿using Application.Contracts;
using Confluent.Kafka;
using Domain.Core.Utility;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Application.Services
{
    public class KafkaProducerService : IKafkaProducerService
    {
        private readonly IProducer<Null, string> kafkaProducer;
        private readonly KafkaConfiguration kafkaConfiguration;

        public KafkaProducerService(IOptions<KafkaConfiguration> kafkaConfiguration)
        {
            this.kafkaConfiguration = kafkaConfiguration.Value;

            var producerConfig = new ProducerConfig
            {
                BootstrapServers = this.kafkaConfiguration.BootstrapServers
            };

            this.kafkaProducer = new ProducerBuilder<Null, string>(producerConfig).Build();
        }

        public async Task ProduceMessageAsync(Guid operationId, string operationName)
        {
            var message = new Message<Null, string>
            {
                Value = JsonConvert.SerializeObject(new KafkaMessageDto { OperationId = operationId, OperationName = operationName })
            };

            await this.kafkaProducer.ProduceAsync(this.kafkaConfiguration.Topic, message);
        }
    }
}
