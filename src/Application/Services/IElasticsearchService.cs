﻿using Application.Contracts;
using Domain.Modules;

namespace Application.Services;

public interface IElasticsearchService
{
    Task InsertPermissionIndexAsync(Permission permission);

    Task InsertPermissionListIndexAsync(List<PermissionsResponse> permissions);
}
