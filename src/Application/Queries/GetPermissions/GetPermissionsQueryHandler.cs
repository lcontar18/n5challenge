﻿using Application.Contracts;
using Application.Core.Abstractions.Data;
using Application.Core.Abstractions.Messaging;
using Application.Services;
using Domain.Modules;
using Microsoft.EntityFrameworkCore;

namespace Application.Queries.GetPermissions;

public sealed class GetPermissionsQueryHandler : IQueryHandler<GetPermissionsQuery, IEnumerable<PermissionsResponse>>
{
    private readonly IChallengeDbContext dbContext;
    private readonly IElasticsearchService elasticsearchService;
    private readonly IKafkaProducerService kafkaProducerService;

    public GetPermissionsQueryHandler(IChallengeDbContext dbContext, IElasticsearchService elasticsearchService, IKafkaProducerService kafkaProducerService)
    {
        this.dbContext = dbContext;
        this.elasticsearchService = elasticsearchService;
        this.kafkaProducerService = kafkaProducerService;
    }

    public async Task<IEnumerable<PermissionsResponse>> Handle(GetPermissionsQuery request, CancellationToken cancellationToken)
    {
        IQueryable<Permission> query = this.dbContext.Set<Permission>();

        if (request.Id != null)
        {
            query = query.Where(x => x.Id == request.Id);
        }

        var result = await query
            .Select(x => new PermissionsResponse(x.Id, x.NombreEmpleado, x.ApellidoEmpleado, x.TipoPermiso, x.FechaPermiso))
            .ToListAsync(cancellationToken);

        if (result != null && result.Any())
        {
            await elasticsearchService.InsertPermissionListIndexAsync(result);
            await kafkaProducerService.ProduceMessageAsync(Guid.NewGuid(), "get");
        }

        return result;
    }
}
