﻿using Application.Contracts;
using Application.Core.Abstractions.Messaging;

namespace Application.Queries.GetPermissions;

public sealed class GetPermissionsQuery : IQuery<IEnumerable<PermissionsResponse>>
{
    public int? Id { get; set; }

    public GetPermissionsQuery(int? id)
    {
        Id = id;
    }
}
