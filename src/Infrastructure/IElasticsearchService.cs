﻿using Domain.Modules;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Core.Abstractions.Data;

public interface IElasticsearchService
{
    Task InsertPermissionAsync(Permission permission);
    Task ModifyPermissionAsync(Permission permission);
    Task<IEnumerable<Permission>> GetPermissionsAsync();
}
