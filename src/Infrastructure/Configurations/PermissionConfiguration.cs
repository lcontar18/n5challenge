﻿using Domain.Core.Primitives;
using Domain.Modules;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Configurations;

internal sealed class PermissionConfiguration : IEntityTypeConfiguration<Permission>
{
    public void Configure(EntityTypeBuilder<Permission> builder)
    {
        builder.ToTable("Permission");

        builder.Property(e => e.ApellidoEmpleado)
                    .IsRequired()
                    .HasMaxLength(100);

        builder.Property(e => e.FechaPermiso).HasColumnType("date");

        builder.Property(e => e.NombreEmpleado)
            .IsRequired()
            .HasMaxLength(100);

        builder.HasOne(d => d.TipoPermisoNavigation)
            .WithMany(p => p.Permissions)
            .HasForeignKey(d => d.TipoPermiso)
            .OnDelete(DeleteBehavior.ClientSetNull)
            .HasConstraintName("FK__Permissio__TipoP__38996AB5");
    }
}
